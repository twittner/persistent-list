// This module is taken from https://github.com/reem/rust-lazy and
// slightly modified. The original license (MIT) applies:
//
// The MIT License (MIT)
//
// Copyright (c) 2015 Jonathan Reem
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use std::ops::{Deref, DerefMut};
use std::boxed::FnBox;
use std::cell::UnsafeCell;
use std::ptr;
use self::Inner::{Ready, InProgress, Deferred};

pub struct Thunk<'r, T> {
    inner: UnsafeCell<Inner<'r, T>>,
}

macro_rules! lazy {
    ($e:expr) => {
        lazy::Thunk::new(move || { $e })
    }
}

enum Inner<'r, T> {
    Ready(T),
    InProgress,
    Deferred(Box<FnBox() -> T + 'r>)
}

impl<'r, T> Thunk<'r, T> {
    pub fn new<F>(f: F) -> Thunk<'r, T> where F: FnOnce() -> T + 'r {
        Thunk {
            inner: UnsafeCell::new(Deferred(Box::new(move || f() )))
        }
    }

    pub fn evaluated<'s>(val: T) -> Thunk<'s, T> {
        Thunk { inner: UnsafeCell::new(Ready(val)) }
    }

    pub fn force(&self) {
        unsafe {
            match *self.inner.get() {
                Ready(_)    => return,
                InProgress  => panic!("Thunk::force called recursively."),
                Deferred(_) => ()
            }
            match ptr::replace(self.inner.get(), InProgress) {
                Deferred(f) => *self.inner.get() = Ready(f()),
                _           => unreachable!()
            }
        }
    }

    pub fn is_evaluated(&self) -> bool {
        unsafe {
            match &*self.inner.get() {
                &Deferred(_) => false,
                _            => true
            }
        }
    }
}

impl<'r, T> Deref for Thunk<'r, T> {
    type Target = T;

    fn deref<'a>(&'a self) -> &'a T {
        self.force();
        unsafe {
            match &*self.inner.get() {
                &Ready(ref val) => val,
                _               => unreachable!()
            }
        }
    }
}

impl<'r, T> DerefMut for Thunk<'r, T> {
    fn deref_mut<'a>(&'a mut self) -> &'a mut T {
        self.force();
        unsafe {
            match &mut *self.inner.get() {
                &mut Ready(ref mut val) => val,
                _                       => unreachable!()
            }
        }
    }
}
