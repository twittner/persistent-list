// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0. If a copy of
// the MPL was not distributed with this file, You
// can obtain one at http://mozilla.org/MPL/2.0/.

//! Immutable singly-linked list.

#![feature(core, alloc)]

#[macro_use]
mod lazy;

use lazy::Thunk;
use std::cmp::{Ord, Ordering, PartialEq};
use std::iter::{IntoIterator, Iterator};
use std::rc;
use std::rc::Rc;

pub struct List<'r, A> {
    node: Option<Rc<Node<'r, A>>>
}

struct Node<'r, A> {
    head: A,
    tail: Thunk<'r, List<'r, A>>
}

pub struct Iter<'r: 'i, 'i, A: 'r> {
    state: &'i List<'r, A>
}

impl<'r, 'i, A> Iterator for Iter<'r, 'i, A> {
    type Item = &'i A;

    fn next(&mut self) -> Option<&'i A> {
        match self.state.node {
            Some(ref n) => {
                self.state = &n.tail;
                Some(&n.head)
            }
            None => None
        }
    }
}

impl<'r, A> List<'r, A> {
    pub fn from_iter<I: 'r + Iterator<Item=A>>(mut iter: I) -> List<'r, A> {
        match iter.next() {
            Some(x) => List {
                node: Some(Rc::new(Node {
                    head: x,
                    tail: lazy!(List::from_iter(iter))
                }))
            },
            None => List::nil()
        }
    }

    pub fn iter<'i>(&'i self) -> Iter<'r, 'i, A> {
        Iter { state: self }
    }

    pub fn nil() -> List<'r, A> {
        List { node: None }
    }

    pub fn cons(&self, x: A) -> List<'r, A> {
        List {
            node: Some(Rc::new(Node {
                head: x,
                tail: Thunk::evaluated(self.clone())
            }))
        }
    }

    pub fn singleton(x: A) -> List<'r, A> {
        List::nil().cons(x)
    }

    pub fn is_empty(&self) -> bool {
        self.node.is_none()
    }

    pub fn hd(&self) -> Option<&A> {
        self.node.as_ref().map(|n| &n.head)
    }

    pub fn tl(&self) -> List<'r, A> {
        self.node.as_ref()
            .map(|n| n.tail.clone())
            .unwrap_or(List::nil())
    }
}

impl<'r, A> Clone for List<'r, A> {
    fn clone(&self) -> List<'r, A> {
        match self.node {
            Some(ref n) => List { node: Some(n.clone()) },
            None        => List::nil()
        }
    }
}

impl<'r, A> Drop for List<'r, A> {
    fn drop(&mut self) {
        let mut curr = self.node.take();
        loop {
            match curr {
                Some(r) => match rc::try_unwrap(r) {
                    Ok(Node { tail: ref mut t, .. }) if t.is_evaluated() =>
                        curr = t.node.take(),
                    _ => return ()
                },
                _ => return ()
            }
        }
    }
}

impl<'r, 'i, A> IntoIterator for &'i List<'r, A> {
    type Item = &'i A;
    type IntoIter = Iter<'r, 'i, A>;

    fn into_iter(self) -> Iter<'r, 'i, A> {
        self.iter()
    }
}

impl<'r, A: 'r + PartialEq> PartialEq for List<'r, A> {
    fn eq(&self, other: &List<'r, A>) -> bool {
        let mut xs = self.iter();
        let mut ys = other.iter();
        for x in &mut xs {
            if Some(x) != ys.next() {
                return false
            }
        }
        xs.next() == ys.next()
    }
}

impl<'r, A: 'r + Eq> Eq for List<'r, A> {}

impl<'r, A: 'r + PartialOrd> PartialOrd for List<'r, A> {
    fn partial_cmp(&self, other: &List<'r, A>) -> Option<Ordering> {
        let mut xs = self.iter();
        let mut ys = other.iter();
        for x in &mut xs {
            match ys.next() {
                Some(y) => {
                    let o = x.partial_cmp(y);
                    if o != Some(Ordering::Equal) {
                        return o
                    }
                }
                None => return Some(Ordering::Greater)
            }
        }
        if ys.next().is_some() {
            Some(Ordering::Less)
        } else {
            Some(Ordering::Equal)
        }
    }
}

impl<'r, A: 'r + Ord> Ord for List<'r, A> {
    fn cmp(&self, other: &List<'r, A>) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

// Tests ////////////////////////////////////////////////////////////////////

#[test]
fn eq() {
    let a = List::from_iter(0 .. 10u32);
    assert!(a == a);
    assert!(a != a.cons(100));
    assert!(a.cons(100) != a);
    assert!(a != List::nil());
    assert!(List::nil() != a)
}

#[test]
fn ord() {
    assert!(List::singleton(1u32) <= List::singleton(1u32));
    assert!(List::singleton(1u32) >= List::singleton(1u32));
    assert!(List::singleton(2u32) > List::singleton(1u32));
    assert!(List::singleton(1u32) < List::singleton(2u32));
    assert!(List::singleton(2u32) > List::singleton(2u32).cons(1u32));
    assert!(List::singleton(2u32).cons(1u32) < List::singleton(2u32))
}

#[test]
fn inf() {
    let inf_list = List::from_iter(1u32 ..);
    assert_eq!(5050, inf_list.iter().take(100).cloned().sum::<u32>())
}

#[test]
fn misc() {
    let a: List<u32> = List::from_iter(1 .. 10);
    let b: List<u32> = List::from_iter(a.iter().map(|x| x + 1).filter(|x| x % 2 == 0));
    assert_eq!(vec![2,4,6,8,10], b.iter().cloned().collect::<Vec<_>>())
}
